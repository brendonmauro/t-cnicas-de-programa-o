#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  rally.py
#  
#  Copyright 2014 ifes <ifes@ifes-serra>
#Construir um programa python para resolver o seguinte problema:
#
#A comissão organizadora de um rally automobilístico decidiu apurar os resultados da competição com o uso do computador.
#O programa deverá classificar as equipes concorrentes e emitir uma listagem geral de desempenho das equipes,
# atribuindo pontos segundo determinadas regras. O programa deverá:
#
#a)Ler os tempos padrões para as 3 fases/etapas da competição;
#
#b)Ler uma quantidade indeterminada de equipes (numero de inscrição, tempo na etapa 1,tempo na etapa 2 e tempo na etapa 3).
# A equipe de inscrição 9999 encerrará a entrada dos dados.
#
#c)Para cada equipe, calcular os pontos conseguidos em cada etapa e o total nas 3 etapas. 
#O critério para o calculo de pontos em cada etapa é o seguinte:
#
# Seja DIF o valor absoluto (sempre positivo) da diferença entre o tempo padrão da etapa e o tempo conseguindo pela 
# equipe na respectiva etapa.
#
# Se:
#    DIF < 3 minutos - pontos igual a 100
#    3<=DIF <= 5 - pontos igual a 80
#    DIF > 5 min - pontos igual a 80 - (DIF-5/5)
#
#d)Ao final, exibir a equipe vencedora.
#
#e)Para cada equipe processada em c), escrever o numero de inscrição, os pontos em cada etapa e o total de pontos.
#
# OBS: * Todos os pontos em minutos.
#      * Criar sobrotinas para os itens A, C e ,D

#inicio da subrotina lertempadrao, onde será armazenado na tabelatempadrao o tempo padrão para cada etapa
def lertempadrao():
	paramtabelatempadrao={}
	for i in range(3):
		print('Etapa',i+1)
		paramtabelatempadrao[i]=int(input('Informe o tempo padrão(em segundos) da etapa:'))
	return paramtabelatempadrao
#fim da subrotina lertempadrao

#inicio da subrotina lerequipes, onde será armazenado na tabela 
#de equipes o tempo de cada etapa e numero de inscrição de cada equipe

def lerequipes():
	temp1=0
	temp2=0
	temp3=0
	paramtabelequipes={}
	numinsc=0
	numinsc= int(input('Informe o número de inscrição[9999 para cancelar]:'))
	while numinsc!=9999:
		temp1=int(input('Informe o tempo da etapa 1 (em segundos)'))
		temp2=int(input('Informe o tempo da etapa 2 (em segundos)'))
		temp3=int(input('Informe o tempo da etapa 3 (em segundos)'))
		paramtabelequipes[numinsc]=(temp1,temp2,temp3)
		numinsc=int(input('Informe o número de inscrição[9999 para cancelar]:'))
	return paramtabelequipes

#fim da subrotina lerequipes

#inicio da subrotina calcpontos que alem de calcular irá impirmir os pontos da equipe
def calcpontos(paramtabelatempadrao,paramtabelequipes):
	dif=0
	maispontos=0.0
	vencedora=''
	pontosequipetapa=0.0
	pontosequipetotal=0.0
	dicpontosequipe={}
	for equipe in paramtabelequipes:
		for i in range(3):
			dif=(paramtabelequipes[equipe][i]/60)-(paramtabelatempadrao[i]/60)
			if dif<3:
				pontosequipetapa=100
			elif dif<=5:
				pontosequipetapa=80
			else:
				pontosequipetapa=80-((dif-5)/5)
			print ('')
			print('A equipe %d conseguiu %.2f na etapa % d' %(equipe,pontosequipetapa,i+1))
			pontosequipetotal+=pontosequipetapa
			if i==2:
				print('O total de pontos da equipe %d é %.2f' % (equipe,pontosequipetotal))
				if pontosequipetotal>maispontos:
					vencedora='A equipe vencedora é '+str(equipe)
					maispontos=pontosequipetotal
				pontosequipetotal=0.0
	print(vencedora)
#fim da subrotina calcpontos

#inicio da subrotina main, que é a subrotina principal onde é executado o programa	
def main():
	tabelatempadrao=lertempadrao()
	tabelequipes=lerequipes()
	calcpontos(tabelatempadrao,tabelequipes)
	
	return 0
#fim da subrotina main
#executar main
if __name__=='__main__':
	main()
