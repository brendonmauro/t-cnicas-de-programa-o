n =int( input("Digite o número de linhas para o triângulo de Pascal: "))
lista = [[1],[1,1]]
for i in range(1,n):
    linha = [1]
    for j in range(0,len(lista[i])-1):
        linha += [ lista[i][j] + lista[i][j+1] ]
    linha += [1]
    lista += [linha]


for i in range(n):
    print('  '*(n-i),end='')
    for j in range(len(lista[i])):
        print(lista[i][j],end='  ')
    print('')
