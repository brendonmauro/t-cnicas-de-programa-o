#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  exercicio5.py
#  
#  Copyright 2014 ifes <ifes@ifes-serra>
#
#Uma empresa de transporte interestadual deseja calcular a distancia percorrida pelos onibus.
#Para isso, é forncecido o percurso de cada onibus com os seguintes dados:
#a)numero do onibus;
#b) quantidade de cidades percorridas;
#c) códigos de todas as cidades percorridas (valores de 1 a 30).

#Assim, podemoster como exemplo a seguinte linha com dados de entrada:
#103,08,01,05,07,03,09,03,08,05

#Que representa o fato de que o onibus de numero 103 percorreu 8 cidades na seguinte sequencia:
#iniciou partindo da cidade 1, da cidade 1 foi até a cidade 05, desta foi até a cidade 07, em seguida foi para a cidade 09, retornou
#à cidade 03, partiu para a cidade 08 e finalmente chegou à cidade 05.

#OBS:
#1- cada onibus pode percorrer no maximo 24 cidades.
#2- Para calcular a distância entre uma cidade e outra, a empresa possui uma tabela de distancia entre cidades (tabela 30 x 30). Veja 
#o exemplo da tabela a seguir:
#   01  02  03 ... 30
#01  0  15  10 ... 90
#02 15   0  25 ... 75
#03 10  25   0 ... 65
#:
#30 45  33  15 ...  0
 
#3- A quantidade de linhas de dados é indeterminada e encerra quando o código do ônibus for igual a 9999.
#4- Tanto as linhas com os dados dos ônibus quando a tabela de distâncias são fornecidas ao sistema via arquivos textos de entrada.
#5- O programa deve produzir a seguinte saída para cada linha de dados lida do arquivo de entrada:
#ônibus numero <nnn> percorreu <mmm>  Km.
#6- Ao final, o programa deve imprimir o código do ônibus que percorreu a maior e a menor distancia entre cidades.
#6- A saída deve ser impressa na tela e também gravada em um arquivo texto de saída.
#7 fazer uso de subrotinas e modelagem de dados com dicionarios e/oui listas.
#FIM
#
# inicio da subrotina lerdadosbus, subrotina esta que irá ler o arquivo dadosbus.txt que contém os dados do onibus
#nº de cidades percorridas e as cidades percorridas

def lerdadosbus():
	paramdicdadosbus={}
	arqdadosbus=open('dadosbus.txt','r')
	linha=arqdadosbus.readline()
	lstlinha=linha.split(',')
	while lstlinha[0]!='9999':
		for i in range(1,len(lstlinha)):
			if i==len(lstlinha)-1:
				lstlinha[len(lstlinha)-1]=lstlinha[len(lstlinha)-1][:-1]
			lstlinha[i]=int(lstlinha[i])
		paramdicdadosbus[lstlinha[0]]=lstlinha[1:]
		linha=arqdadosbus.readline()
		lstlinha=linha.split(',')
	arqdadosbus.close()
	return paramdicdadosbus

#fim da subrotina lerdadosbus

#inicio da subrotina lertabeladistancia, subrotina esta que irá ler o arquivo tabeladistancia.txt que contém
#uma tabela 30x30 com cada distancia de cidade para cidade

def lertabeladistancia():
	paramtabeladistancia=[]
	arqtabeladistancia=open('tabeladistancia.txt','r')
	for i in range(30):
		linha=arqtabeladistancia.readline()
		lstlinha=linha.split()
		for j in range(30):
			lstlinha[j]=float(lstlinha[j])
		paramtabeladistancia+=[lstlinha]
	arqtabeladistancia.close()
	return paramtabeladistancia

#fim da subrotina lertabeladistancia

#inicio da subrotina calcdistanciatotal

def calcdistancia(paramdicdadosbus,paramtabeladistancia):
	arqresultadodistancias=open('resultadodistancias.txt','w')
	menor=9999999999999999999
	maior=0
	codmaior=''
	codmenor=''
	for cod in paramdicdadosbus:
		distanciatotal=0.0	
		for i in range(1,paramdicdadosbus[cod][0]):
			cidadeA=paramdicdadosbus[cod][i]
			cidadeB=paramdicdadosbus[cod][i+1]
			distancia=paramtabeladistancia[cidadeA-1][cidadeB-1]
			distanciatotal+=distancia
		print('O ônibus numero %s percorreu %.2f  Km.'%(cod,distanciatotal))
		strdistanciatotal=str(distanciatotal)
		arqresultadodistancias.writelines('Ônibus numero %s percorreu %s  Km. \n'%(cod,strdistanciatotal))
		if distanciatotal>maior:
			maior=distanciatotal
			codmaior=cod
		if distanciatotal<menor:
			menor=distanciatotal
			codmenor=cod
	print('O ônibus numero %s percorreu a maior distancia.'%(codmaior))
	print('O ônibus numero %s percorreu a menor distancia.'%(codmenor))
	arqresultadodistancias.writelines('O ônibus numero %s percorreu a maior distancia. \n'%(codmaior))
	arqresultadodistancias.writelines('O ônibus numero %s percorreu a menor distancia.'%(codmenor))
	arqresultadodistancias.close()
	return 0

#fim da subrotina calcdistanciatotal

#inicio da subrotina principal main()

def main():
	dicdadosbus=lerdadosbus()
	tabeladistancia=lertabeladistancia()
	calcdistancia(dicdadosbus,tabeladistancia)
	return 0
	
#fim da subrotina main 

#executar main()
if __name__=='__main__':
	main()