#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  mercadorias.py
#  
#  Copyright 2014 ifes <ifes@ifes-serra>
#
#-contrua um programa python para o seguinte cenário:
#-uma entrada de estoque é composta por um código de mercadoria e a quantidade existente da mercadoria.
#mercadoria e a quantidade existente da mercadoria.
#uma tabela de controle de estoque é formada por várias entradas de estoque.
#- o programa python deve fazer o seguinte:
#a) ler a tabela de controle de estoque inicial:
#(cerca de 100 entradas/mercadorias).
#crie uma subrotina para ler e retornar essa tabela.
#b)ler uma quantidade indeterminada de peldidos de clientes. cada pedido composto por número do
#cliente, código da mercadoia e quantidade desejada. Código do cliente igual a 9999 encerra a entrada dos pedidos.

#c) Para cada pedido, seja verificado se ele pode ser atendido (se existe quantidade disponível no estoque). 
#Em caso positivo, exibir o código do cliente, o código da mercadoria e a frase 'pedido processado'. Em caso negativo,
#imprimir o código do cliente e a mensagem 'não temos a mercadoria em estoque suficiente.
#d) Após cada operação de atendimento ao cliente, o estoque deve ser atualizado.
#e) AO final, listar a tabela de estoque com os valores atualizados.
#(crie subrotinas para as letras c), d) e e)).

#inicio da subrotina lertabela que irá ler tabela de controle 
def lertabela():
		codmerc=''
		quantmerc=0
		tabelcontrol={}
		lstcodmerc=[]
		for i in range(1):
			codmerc=input('Informe o código da mercadoria:')
			lstcodmerc+=[codmerc]
			quantmerc=int(input('Informe a quantidade existente da mercadoria:'))
			tabelcontrol[codmerc]=quantmerc
		return tabelcontrol,lstcodmerc
#fim da subrotina lertabela

#inicio da subrotina verifipedido que verificará se há estoque para tal pedido
def verifipedido(tabelcontrol,codmerc,codclient,paramqtdesej):
       if not(paramqtdesej>tabelcontrol[codmerc]):
               print(codclient)
               print(codmerc)
               print('PEDIDO PROCESSADO')
               verifi=True
       else:
               print(codclient)
               print('NAO TEMOS A MERCADORIA EM ESTOQUE SUFICIENTE')
               verifi=False
       return(verifi)
#fim da subrotina verifipedido

#inicio da subrotina atualiEstok que atualizará o estoque assim que o pedido for processado
def atualiEstok(tabelcontrol,codmerc,qtdesej):
        tabelcontrol[codmerc]-=qtdesej
#fim da subrotina atualiEstok

#inicio da subrotina listaTabela que tem como objetivo listar as mercadorias e a quantidade em estoque
def listaTabela(tabelcontrol,lstcodmerc):
	for i in range(len(lstcodmerc)):
		print('A mercadoria com código:',lstcodmerc[i], 'está com a quantidade em estoque:',tabelcontrol[lstcodmerc[i]])
#fim de subrotina listaTabela

#inicio da subrotina main, que é a subrotina principal onde é executado o programa			
def main():
	tabelcontrol,lstcodmerc=lertabela()
	codclient=int(input('Informe o código do cliente:'))
	while codclient!=9999:
		codmerc=input('Informe o código da mercadoria desejada:')
		qtdesej=int(input('Informe a qtd desejada:'))
		verifi=verifipedido(tabelcontrol,codmerc,codclient,qtdesej)
		if verifi:
			atualiEstok(tabelcontrol,codmerc,qtdesej)
		codclient=int(input('Informe o código do cliente:'))
	listaTabela(tabelcontrol,lstcodmerc)
	return 0
#fim da subrotina main
#executar main
if __name__ == '__main__':
	main()
