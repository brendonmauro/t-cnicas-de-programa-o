#!/usr/bin/env python
#-*- coding:UTF-8 -*-
#
# contfreqpal.py
#
# Copyright 2013 Brendon Mauro <ifes@ifes-serra>
################################################################################

#declaração de variaveis

dic={} # dicionario que vai conter as palavras sem repetição
lstpal=[] #lista de todas as palavras do texto
texto,letra,palavra='','','' #aqui temos a variavel que vai receber o texto, a outra caracteres(ou letras)
#do texto e a outra recebe as palavras e manda para a lista(com repetição) e para o dicionario(sem repeticao)

cont=0 #essa variavel será o contador de palavras do dicionario e servirá de chave do dicionario
separadores=' .,;:!?()' #separadores de palavras muito util para ver onde a palavra terminou
contfreq=[] #contador de frequencia

#Inicio

#Leitura do texto

print('Digite o texto:')
texto=input()
texto+=' ' #um espaçamento no final para auxiliar no armazenamento de palavras

# alocando palavras no dicionario e na lista de palavras

for letra in texto:
    if not(letra in separadores):
        palavra+=letra
    elif len(palavra)>0:
        if not(palavra in lstpal):
            dic[cont]=palavra
            contfreq.append(0) #aqui nao esta ocorrendo a contagem de frequencia, mas abrindo o espaço necessario para a contagem
            cont+=1
        lstpal.append(palavra)
        palavra=''

#contando a frequencia de cada palavra
        
for cont in range(len(dic)):
    for palavra in lstpal:
        if palavra==dic[cont]:
            contfreq[cont]+=1

#imprimindo o cabeçalho
print('')
print(' Palavra                    | Frequencia')
print(' ---------------------------------------')

#imprimindo as palavras e a suas respectivas frequencias
for cont in range(len(dic)):
    print(' ',dic[cont],' '*(24-(len(dic[cont]))),'|       ',contfreq[cont])
    
#fim do programa
