#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# acentosv1.py
#
# Copyright 2013 Brendon Mauro <ifes@ifes-serra
#
# Python 3.x
#####################################################################

#declaração de variavéis

texto='';palavacen='áéíóúâêîôûàèìòùãõ';corrigi='aeiouaeiouaeiouao';textocorrigi=''

#inicio

print('Escreva o texto:')
texto=input()

for i in range (len(texto)):
    achou=False
    if texto[i] in palavacen:
        for j in range(17):
            if texto[i]==palavacen[j]:
                textocorrigi+=corrigi[j]
                achou=True
                break
    if not(achou):
        textocorrigi+=texto[i]
print()
print('O seu texto sem acento fica assim:')
print(textocorrigi)

#fim
