#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  reservavoo.py
#  
#  Copyright 2014 ifes <ifes@ifes-serra>
#Exercícios
#- Construir um programa python para controlar as reservas de passagens de uma companhia aérea e verificar os lucrose prejuízos da mesma. O programa deverá:
#    1. Ler os dados de um numero indeterminado de voos. Os dados de cada voo são formados pelo: número do voo, tipo de aviao utilizado(707,727,757) e preço da passagem.
#    2. Ler um número indeterminado de pedidos de reservas contendo cada um: número da identidade do passageiro e voo desejado(identidade igual a 0 é o flag de fim de entrada)
#    3. Verificar para cada passageiro se há disponibilidade no voo. Em caso afirmativo, atualizar o numero de lugares disponíveis e imprimir: 
#        -> Identidade do passageiro.
#        -> Número do voo.
#        -> Preço da passagem.
#        -> A mensagem 'Reserva Confirmada'.
#        -> Em caso negativo, imprimir a identidade do passageiro, o numero do voo e a mensagem 'voo lotado'
#    4. Ao final do processamento, imprimir uma estatistica de lucro e prejuízo por voo. Considerar que lotação acima de 60%(por voo) produz lucro e abaixo de 60% produz prejuízo.

#OBS.: Capacidade de cada modelo de avião: 
#707 - 200 lugares
#727 - 170 lugares  -----> Dicionário!
#737 - 120 lugares


#Resolução logica
#1 - ler a tabela de aeronaves [sub rotina]
#2 - ler a tabela de voos      [sub rotina] #Dicionário do tipo {<chave>,<conteudo>} cujo conteudo serao 3 coisas diferentes
#3 - venda das passagens
#inicio da subrotina lertabaeros, com o objetivo de ler a tabela de aeronaves e retorna-la a uma variavel na subrotina main

def lertabaeros():
	paramdictabaeros={} # parametro do dicionario da tabela de aeronaves
	paramdiclugarini={} # parametro do dicionario com o numero de lugares inicial de lugares(porque depois o numero de lugares será tirado com as reservas na outra tabela)
	tipo='' #variavel que indica o tipo do aviao
	lugares=0 #variavel que armazenara o numero de lugares
	tipo=input('Informe o tipo de aeronave[FIM para cancelar]:') #leitura do tipo de aeronave
	while tipo!='FIM': #while para repetição da leitura de aeronaves com seu respectivos tipos e lugares
		lugares=int(input('Informe o número de lugares desta aeronave:'))
		paramdiclugarini[tipo]=lugares #armazenamento do tipo e lugares inicialmente
		paramdictabaeros[tipo]=lugares # armazenamento na tabela aeronaves o tipo e lugares
		tipo=input('Informe o tipo de aeronave[FIM para cancelar]:')
	return paramdictabaeros,paramdiclugarini #retorna tabela de aeronaves e o dicionario com o numero de lugares que não será modificado

#fim da subrotina lertabaeros	
	
#Inicio da subrotina lertabvoos esta subrotina irá ler a tabela de voos

def lertabvoos(paramdictabaeros): #necessitara do parametro da tabela dictabaeros, através do tipo de aeronave já entregue na tabela de aeronaves será retornado o numero de lugares
	paramdictabvoos={} #dicionario (parametro) que irá ser uma tabela de voos
	paramlstvoos=[] #lista de voos que será de utilidade abaixo para saber se obter cada voo já registrado
	numvoo=0  #variavel que armazenará o numero de voo
	preco=0.0 #variavel que armazenará o preco
	numvoo=int(input('Informe o numero do voo[numero negativo para cancelar]:'))
	while numvoo>=0: #repetições onde será criada a tabela de voos
		paramlstvoos+=[numvoo]
		tipo=input('Informe o tipo de aeoronave a ser usada neste voo:')
		lugares=paramdictabaeros[tipo]
		preco=float(input('Informe o preço da passagem:'))
		paramdictabvoos[numvoo]=[tipo,preco,lugares] #armazenando na tabela de voos o numero do voo como chave e em uma lista o tipo, preço e lugares como valor
		numvoo=int(input('Informe o numero do voo[numero negativo para cancelar]:'))
	return paramdictabvoos,paramlstvoos

#fim da subrotina lertabvoos

#inicio da subrotina temlugar, que avaliará se voo ainda tem lugar
def temlugar(paramdictabvoos,paramnumvoo):
        return paramdictabvoos[paramnumvoo][2]>0
 
#fim da subrotina temlugar

#inicio da subrotina reservalugar que tem como objetivo reservar um lugar e tirar mais um lugar no voo
def reservalugar(paramdictabvoos,paramnumvoo):
    paramdictabvoos[paramnumvoo][2] -= 1
    return paramdictabvoos
#fim da subrotina reservalugar

#inicio da subrotina estaticfim que dará a estatisca final se o voo produziu ou não lucro
def estaticfim(paramdictabvoos,paramdiclugarini,paramlstvoos):
	for i in range(len(paramlstvoos)):
		contvoo=(paramdiclugarini[paramdictabvoos[paramlstvoos[i]][0]])-(paramdictabvoos[paramlstvoos[i]][2])
		if contvoo>=(60/100)*(paramdiclugarini[paramdictabvoos[paramlstvoos[i]][0]]):
			print('O voo',paramlstvoos[i],'produziu lucro.')
		else:
			print('O voo',paramlstvoos[i],'não produziu lucro.')
		contvoo=[]
#fim da subrotina estaticfim

#inicio da subrotina main, que é a subrotina principal onde é executado o programa
def main():
	RG,VOO = 0, 1
	dictabaeros = {}   #dicionário de aeoronaves principal(main)
	dictabvoos = {}   #dicionário de voos principal(main)
	reserva = []       # variavel responsavel pelo processo de reserva
	contvoo=0
	print('Entre com a tabela de aeronaves: ')
	dictabaeros,diclugarini=lertabaeros()
	print('Entre com a tabela de voos: ')
	dictabvoos,lstvoos= lertabvoos(dictabaeros)
	reserva += [int(input('Entre com o RG: '))]
	while reserva[RG] != 0:
		reserva += [int(input('Entre com o numero do voo: '))]
		vervoo=temlugar(dictabvoos,reserva[VOO])
		if vervoo:
			dictabvoos=reservalugar(dictabvoos,reserva[VOO])
			print('RG:',reserva[RG])
			print('voo',reserva[VOO])
			print('RESERVA CONFIRMADA.')
		else:
			print('RG:',reserva[RG])
			print('Nº do voo:',reserva[VOO])
			print('VOO LOTADO.')
		reserva = []
		reserva += [int(input('Entre com o RG: '))]
	estaticfim(dictabvoos,diclugarini,lstvoos)
	return 0
#fim da subrotina main

#executar main
if __name__ == '__main__':
	main()
