#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# stopword.py
#
# Copyright 2014 ifes <ifes@ifes-serra>
#
#fazer um programa que devolve o texto removendo as stopwords.
#

#inicio da subrotina lestopwords que irá ler as stopwords em um arquivo separado
def lestopwords():
	auxlst= []
	arqstopw=open('swpb.txt','rt',encoding='utf-8')
	conteudo=arqstopw.readline()
	while conteudo!='':
		conteudo=conteudo[:-2]
		auxlst.append(conteudo)
		conteudo=arqstopw.readline()
	arqstopw.close()
	return auxlst
# fim de lestopwords

#inicio da subrotina inserebrancos, que irá inserir brancos para que as palavras não sejam guardadas com virgulas
def inserebrancos(frase):
	straux=''
	for caractere in frase:
		if caractere in '.,;:?!':
			straux=straux+' '+caractere+' '
		else:
			straux= straux+caractere
	return straux

#fim da subrotina inserebrancos

#inicio da subrotina removesw, subrotina esta que irá remover as stopwords	
def removesw(paramconteudo,paramlstsw):
	paramconteudo= inserebrancos(paramconteudo)
	lstpals=paramconteudo.split()
	auxconteudo= ''
	paramqtdsw=0
	for palavra in lstpals:
		if not(palavra.lower() in paramlstsw):
			auxconteudo=auxconteudo + palavra+' '
		else:
			paramqtdsw+=1
	return auxconteudo,paramqtdsw
#fim da subrotina removesw
	
#inicio da subrotina main, que é a subrotina principal onde é executado o programa		
def main():
	lstsw=lestopwords()
	quantsw=0
	auxlst=[]
	totalsw=0
	arqtextoul= open('texto.txt','rt',encoding='utf-8')
	arqtxtuolsemsw= open('textoulsemsw.txt','wt',encoding='utf-8')
	conteudo=arqtextoul.readline()
	while conteudo!='':
		conteudo=conteudo[:-2] #remove o \n final
		conteudosemsw,quantsw=removesw(conteudo,lstsw)
		totalsw=totalsw+quantsw
		arqtxtuolsemsw.write(conteudosemsw + '\n')
		auxlst.append(conteudo)
		conteudo=arqtextoul.readline()
	arqstopw=open('swpb.txt','rt',encoding='utf-8')
	arqtextoul.close()
	arqtxtuolsemsw.close()
	print('Concluido: Total de %d stopwords  encontradas.' %(totalsw))
	return 0
#fim da subrotina main
#executar main
if __name__=='__main__':
        main()
