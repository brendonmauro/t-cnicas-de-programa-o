#declaração de variavéis

textorec='';textofim=''

#inicio
def removacento(texto):
	corrigi='aeiouaeiouaeiouao'
	palavacen='áéíóúâêîôûàèìòùãõ'
	textocorrigi=''
	for letra in texto:
		achou=False
		if letra in palavacen:
			for j in range(17):
				if letra==palavacen[j]:
					textocorrigi+=corrigi[j]
					achou=True
					break
		if not(achou):
			textocorrigi+=letra	
	return textocorrigi
def main():
	print('Escreva o texto:')
	textorec=input()
	textofim=removacento(textorec)
	print()
	print('O seu texto sem acento fica assim:')
	print(textofim)
	return 0
if __name__=='__main__':
	main()

#fim
